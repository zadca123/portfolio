from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

import uvicorn
from pathlib import Path
from sqladmin import Admin

from views import home, account, exercise, lesson
from data import db_session, admin_models


app = FastAPI()
db_file = Path(__file__).parent.joinpath("db", "pypi.sqlite").absolute().as_posix()


def main():
    configure()
    uvicorn.run(app, port=8000, host="127.0.0.1")


def configure():
    app.mount("/static", StaticFiles(directory="static"), name="static")
    configure_route()
    configure_db(dev_mode=True)
    configure_admin()


def configure_route():
    app.mount("/static", StaticFiles(directory="static"), name="static")
    app.include_router(home.router)
    app.include_router(account.router)
    app.include_router(exercise.router)
    app.include_router(lesson.router)


def configure_db(dev_mode: bool):
    db_session.global_init(db_file)
    print(db_file)


def configure_admin():
    admin = Admin(app, db_session.get_engine(db_file))
    admin.register_model(admin_models.UserAdmin)
    admin.register_model(admin_models.ExerciseAdmin)
    admin.register_model(admin_models.LessonAdmin)


if __name__ == "__main__":
    main()
else:
    configure()
