from starlette.requests import Request
from viewmodels.shared.viewmodel import ViewModelBase

# from services import user_service, package_service


class IndexViewModel(ViewModelBase):
    def __init__(self, request: Request):
        super().__init__(request)

        # self.exercises: int = package_service.release_count()
        # self.registered_users: int = user_service.registered_users()
        # self.exercises_taken: int = package_service.package_count()
        # self.exercises_done: list = package_service.packages(limit=5)
