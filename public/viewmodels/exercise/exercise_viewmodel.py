from fastapi import Request
from viewmodels.shared.viewmodel import ViewModelBase
from services import exercise_service


class ExerciseViewModel(ViewModelBase):
    # def __init__(self, package_name: str, request: Request):
    def __init__(self, request: Request):
        super().__init__(request)

        self.exercises = exercise_service.get_all()
        self.exercises_count = exercise_service.get_count()
        self.languages = exercise_service.get_languages()
        # self.answer = exercise_service.get_answer_by_id()
        # self.exercise = exercise_service.get_id_by()

    # def exercises_by_lang(self, lang: str):
    #     return exercise_service.get_by_lang(lang)

    async def load(self, ex_id: int):
        form = await self.request.form()
        self.user_answer = form.get("answer", "")

        if self.user_answer != exercise_service.get_by_id(ex_id).answer:
            self.error = "Odpowiedź niepoprawna!"

    # async def load(self):
    #     form = await self.request.form()
    #     self.name = form.get("name")
    #     self.password = form.get("password")
    #     self.email = form.get("email")

    #     if not self.name or not self.name.strip():
    #         self.error = "Your name is required."
    #     elif not self.email or not self.email.strip():
    #         self.error = "Your email is required."
    #     elif not self.password or len(self.password) < 5:
    #         self.error = "Your password is required and must be at 5 characters."
    #     elif user_service.get_user_by_email(self.email):
    #         self.error = "That email is already taken. Log in instead?"
