# noinspection PyUnresolvedReferences
from data.user import User

# noinspection PyUnresolvedReferences
from data.exercise import Exercise

# noinspection PyUnresolvedReferences
from data.lesson import Lesson
