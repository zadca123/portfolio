from sqladmin import ModelAdmin
from data.__all_models import User, Exercise, Lesson


class UserAdmin(ModelAdmin, model=User):
    can_create = True
    can_edit = True
    can_delete = True
    can_view_details = True

    column_list: list = [User.id, User.name, User.email]
    # column_exclude_list: list = [User.id]
    # column_details_exclude_list: list = [User.id]
    # column_details_exclude_list: list = [User.id]


class ExerciseAdmin(ModelAdmin, model=Exercise):
    column_list: list = [
        Exercise.id,
        Exercise.question,
        Exercise.answer,
        Exercise.language,
        Exercise.difficulty,
    ]


class LessonAdmin(ModelAdmin, model=Lesson):
    column_list: list = [
        Lesson.id,
        Lesson.summary,
        Lesson.topic,
    ]
