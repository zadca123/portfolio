import datetime
import sqlalchemy as sa

from data.modelbase import SqlAlchemyBase


class User(SqlAlchemyBase):
    __tablename__ = "users"

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    name = sa.Column(sa.String)
    email = sa.Column(sa.String, index=True, unique=True)
    hash_password = sa.Column(sa.String)
    created_date = sa.Column(sa.DateTime, default=datetime.datetime.now)
    last_login = sa.Column(sa.DateTime, default=datetime.datetime.now)
    profile_image_url = sa.Column(sa.String)
    # points = sa.Column(sa.Integer, default=0)
    # ex_done = sa.Column(sa.Enum())
