import sqlalchemy as sa
import sqlalchemy.orm as orm
from data.modelbase import SqlAlchemyBase, all_languages, all_difficulties, all_topics


class Exercise(SqlAlchemyBase):
    __tablename__ = "exercise"

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    question = sa.Column(sa.String, nullable=False, default="Def question")
    answer = sa.Column(sa.String, nullable=False, default="Def answer")
    language = sa.Column(sa.Enum(*all_languages), nullable=False)
    difficulty = sa.Column(sa.Enum(*all_difficulties), nullable=False)
    topic = sa.Column(sa.Enum(*all_topics), nullable=False)

    # summary = sa.Column(sa.String, nullable=False, default="TEST")
    # Language relationship
    # language_id = sa.Column(sa.String, sa.ForeignKey("language.name"))
    # language = orm.relationship("Language")
