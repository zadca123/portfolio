import sqlalchemy as sa
import sqlalchemy.orm as orm
from data.modelbase import (
    SqlAlchemyBase,
    all_languages,
    all_difficulties,
    all_topics,
    lorem_ipsum,
)


class Lesson(SqlAlchemyBase):
    __tablename__ = "lesson"

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    content = sa.Column(sa.Text, nullable=False, default=lorem_ipsum)
    summary = sa.Column(sa.String, nullable=False, default="SUMMARY")
    language = sa.Column(sa.Enum(*all_languages), nullable=False)
    difficulty = sa.Column(sa.Enum(*all_difficulties), nullable=False)
    topic = sa.Column(sa.Enum(*all_topics), nullable=False)

    # Language relationship
    # language_id = sa.Column(sa.String, sa.ForeignKey("language.name"))
    # language = orm.relationship("Language")
