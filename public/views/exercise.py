from fastapi import APIRouter, Request, Response
from starlette.templating import Jinja2Templates
from viewmodels.account.account_viewmodel import AccountViewModel

from viewmodels.shared.viewmodel import ViewModelBase
from viewmodels.exercise.exercise_viewmodel import ExerciseViewModel
from services import exercise_service

templates = Jinja2Templates("templates")
router = APIRouter()


@router.get("/education")
def education(request: Request):
    vm = ExerciseViewModel(request)
    return templates.TemplateResponse(
        "./education/index.jinja2",
        {"request": request, **vm.to_dict()},
    )


@router.get("/education/exercise/")
def exercises(request: Request):
    vm = ViewModelBase(request)
    if not vm.is_logged_in:
        return "you are not logged in!"

    response = templates.TemplateResponse(
        "./education/exercise.jinja2",
        {"request": request, **vm.to_dict()},
    )
    return response


@router.get("/education/exercise/{ex_id}")
def exercise_get(request: Request, ex_id: int):
    vm = ExerciseViewModel(request)
    # response = templates.TemplateResponse(
    #     "./education/exercise.jinja2",
    #     {"request": request, "ex_id": ex_id},
    # )

    exercise = exercise_service.get_by_id(ex_id)
    response = templates.TemplateResponse(
        "./education/exercise.jinja2",
        {"request": request, "exercise": exercise},
    )
    return response


@router.post("/education/exercise/{ex_id}")
async def exercise_post(request: Request, ex_id: int):
    vm = ExerciseViewModel(request)
    await vm.load(ex_id)
    if vm.error:
        return templates.TemplateResponse(
            "./education/exercise.jinja2",
            {"request": request, **vm.to_dict()},
        )
    return "Odpowiedź poprawna"
