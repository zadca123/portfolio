from fastapi import APIRouter, Request
from starlette.templating import Jinja2Templates

from viewmodels.home.indexviewmodel import IndexViewModel

templates = Jinja2Templates("templates")
router = APIRouter()


@router.get("/")
@router.get("/home")
def home(request: Request):
    vm = IndexViewModel(request)
    return templates.TemplateResponse(
        "./home/index.jinja2",
        {"request": request, **vm.to_dict()},
    )


@router.get("/about")
def about(request: Request):
    vm = IndexViewModel(request)
    return templates.TemplateResponse(
        "./home/about.jinja2",
        {"request": request, **vm.to_dict()},
    )
