from fastapi import APIRouter, Request, Response
from starlette.responses import RedirectResponse
from starlette.templating import Jinja2Templates
from starlette.status import HTTP_302_FOUND
from services.user_service import create_account

from viewmodels.account.register_viewmodel import RegisterViewModel
from viewmodels.account.login_viewmodel import LoginViewModel
from viewmodels.account.account_viewmodel import AccountViewModel

from infrastructure import cookie_auth
from services import user_service

templates = Jinja2Templates("templates")
router = APIRouter()


@router.get("/account")
def account(request: Request):
    vm = AccountViewModel(request)
    return templates.TemplateResponse(
        "./account/index.jinja2",
        {"request": request, **vm.to_dict()},
    )


@router.get("/account/register")
def register_get(request: Request):
    vm = RegisterViewModel(request)
    return templates.TemplateResponse(
        "./account/register.jinja2",
        {"request": request, **vm.to_dict()},
    )


@router.post("/account/register")
async def register_post(request: Request):
    vm = RegisterViewModel(request)
    await vm.load()
    if vm.error:
        return templates.TemplateResponse(
            "./account/register.jinja2",
            {"request": request, **vm.to_dict()},
        )

    # Create an Account
    account = create_account(vm.name, vm.email, vm.password)

    # TODO: Login user
    response = RedirectResponse(url="/account", status_code=HTTP_302_FOUND)
    cookie_auth.set_auth(response, account.id)

    return response


@router.get("/account/login")
def login_get(request: Request):
    vm = LoginViewModel(request)
    return templates.TemplateResponse(
        "./account/login.jinja2",
        {"request": request, **vm.to_dict()},
    )


@router.post("/account/login")
async def login_post(request: Request):
    vm = LoginViewModel(request)
    await vm.load()

    if vm.error:
        return templates.TemplateResponse(
            "./account/login.jinja2",
            {"request": request, **vm.to_dict()},
        )

    user = user_service.login_user(vm.email, vm.password)
    if not user:
        vm.error = "The account does not exist or the password is wrong."
        return templates.TemplateResponse(
            "./account/login.jinja2",
            {"request": request, **vm.to_dict()},
        )

    response = RedirectResponse("/account", status_code=HTTP_302_FOUND)
    cookie_auth.set_auth(response, user.id)
    return response


@router.get("/account/logout")
def logout(request: Request):
    response = RedirectResponse(url="/", status_code=HTTP_302_FOUND)
    cookie_auth.logout(response)
    return response
