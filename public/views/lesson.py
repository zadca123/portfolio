from fastapi import APIRouter, Request, Response
from starlette.templating import Jinja2Templates
from viewmodels.account.account_viewmodel import AccountViewModel

from viewmodels.shared.viewmodel import ViewModelBase
from viewmodels.exercise.exercise_viewmodel import ExerciseViewModel

templates = Jinja2Templates("templates")
router = APIRouter()


@router.get("/education/lesson")
def lessons(request: Request):
    vm = ExerciseViewModel(request)
    return templates.TemplateResponse(
        "./education/index.jinja2",
        {"request": request, **vm.to_dict()},
    )


@router.get("/education/exercise/")
def exercises(request: Request):
    account = AccountViewModel(request)
    vm = ViewModelBase(request)
    if not account.is_logged_in:
        return "you are not logged in!"

    response = templates.TemplateResponse(
        "./education/exercise.jinja2",
        {"request": request, **account.to_dict()},
    )
    return response


@router.get("/education/exercise/{ex_id}")
def exercise_get(request: Request, ex_id: int):
    response = {}

    return response


@router.post("/education/exercise/{ex_id}")
def exercise_post(response: Response, ex_id: int):
    return templates.TemplateResponse(
        "./education/write.jinja2",
        {"response": response, "ex_id": ex_id},
    )
