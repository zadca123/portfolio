function myAccFunc(id) {
    var x = document.getElementById(id);
    x.classList.toggle("w3-show");
    x.previousElementSibling.classList.toggle("w3-green");
}

function highlightCurrentActive(id) {
    let x = document.getElementById(id);
    x.classList.toggle("active");
    x.previousElementSibling.classList.toggle("active");
}

function myDropFunc(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
        x.previousElementSibling.className += " w3-green";
    } else { 
        x.className = x.className.replace(" w3-show", "");
        x.previousElementSibling.className = 
            x.previousElementSibling.className.replace(" w3-green", "");
    }
}
