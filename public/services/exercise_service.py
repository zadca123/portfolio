from typing import List, Optional

import sqlalchemy.orm
from sqlalchemy.orm.query import Query

from data import db_session
from data.exercise import Exercise


def get_all() -> list[Exercise]:
    session = db_session.create_session()
    try:
        return session.query(Exercise).all()
    finally:
        session.close()


def get_by_id(id_num: int) -> Exercise:
    session = db_session.create_session()
    try:
        package = session.query(Exercise).filter(Exercise.id == id_num).first()
        return package
    finally:
        session.close()


def get_by_lang(lang: str) -> list[Exercise]:
    session = db_session.create_session()
    try:
        return session.query(Exercise).filter(Exercise.language == lang)
    finally:
        session.close()


def get_count() -> int:
    session = db_session.create_session()
    try:
        return session.query(Exercise).count()
    finally:
        session.close()


def get_languages() -> list[str]:
    session = db_session.create_session()
    try:
        return session.query(Exercise.language).distinct().order_by(Exercise.language)
    finally:
        session.close()


# def latest_packages(limit: int = 5) -> List[Exercise]:
#     session = db_session.create_session()

#     try:
#         releases = (
#             session.query(Exercise)
#             .options(sqlalchemy.orm.joinedload(Exercise.package))
#             .order_by(Exercise.created_date.desc())
#             .limit(limit)
#             .all()
#         )
#     finally:
#         session.close()

#     return list({r.package for r in releases})


# def get_latest_release_for_package(package_name: str) -> Optional[Exercise]:
#     session = db_session.create_session()

#     try:
#         release = (
#             session.query(Exercise)
#             .filter(Exercise.package_id == package_name)
#             .order_by(Exercise.created_date.desc())
#             .first()
#         )

#         return release
#     finally:
#         session.close()
